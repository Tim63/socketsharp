﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
#endregion

namespace SocketSharp
{
    /// <summary>
    /// ソケットを利用したサーバー
    /// </summary>
    public class Server : IDisposable
    {
        #region Constants / Readonlies
        private readonly object SyncObject = new object();
        #endregion

        #region Events
        /// <summary>
        /// 接続を受け付けた時のイベント
        /// </summary>
        public event EventHandler<AcceptedEventArgs> Accepted;

        /// <summary>
        /// 接続の受け付けに失敗した時のイベント
        /// </summary>
        public event EventHandler<AcceptFailedEventArgs> AcceptFailed;

        /// <summary>
        /// クライアントの接続が閉じた時に発生するイベント
        /// </summary>
        public event EventHandler<ConnectionEventArgs> ClientClosed;

        /// <summary>
        /// データを受信した時に発生するイベント
        /// </summary>
        public event EventHandler<DataReceivedEventArgs> DataReceived;
        #endregion

        #region Properties
        /// <summary>
        /// ローカルエンドポイント
        /// </summary>
        public IPEndPoint LocalEndPoint { get; private set; }

        /// <summary>
        /// アドレスファミリ
        /// </summary>
        public AddressFamily AddressFamily { get; private set; }

        /// <summary>
        /// ソケットの種類
        /// </summary>
        public SocketType SocketType { get; private set; }

        /// <summary>
        /// 接続プロトコルの種類
        /// </summary>
        public ProtocolType ProtocolType { get; private set; }

        /// <summary>
        /// 利用する文字エンコーディング
        /// </summary>
        public Encoding Encoding { get; private set; }

        /// <summary>
        /// 受信バッファサイズ
        /// </summary>
        public int ReceiveBufferSize { get; private set; }

        /// <summary>
        /// サーバーの状態
        /// </summary>
        public ConnectionState ServerState { get; private set; }
        #endregion

        #region Fields
        /// <summary>
        /// 接続受付用ソケット
        /// </summary>
        private Socket listenSocket;

        /// <summary>
        ///　確立した接続
        /// </summary>
        private List<Connection> connections;
        #endregion

        #region Create / Dispose
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="addressFamily">アドレスファミリ</param>
        /// <param name="socketType">ソケットの種類</param>
        /// <param name="protocolType">接続プロトコルの種類</param>
        /// <param name="receiveBufferSize">受信バッファサイズ</param>
        public Server(AddressFamily addressFamily = AddressFamily.InterNetwork, SocketType socketType = SocketType.Stream, ProtocolType protocolType = ProtocolType.Tcp, int receiveBufferSize = NetworkAsyncState.DefaultReceiveBufferSize)
            : this(Encoding.UTF8, addressFamily, socketType, protocolType, receiveBufferSize)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="encoding">文字エンコーディング</param>
        /// <param name="addressFamily">アドレスファミリ</param>
        /// <param name="socketType">ソケットの種類</param>
        /// <param name="protocolType">接続プロトコルの種類</param>
        /// <param name="receiveBufferSize">受信バッファサイズ</param>
        public Server(Encoding encoding, AddressFamily addressFamily = AddressFamily.InterNetwork, SocketType socketType = SocketType.Stream, ProtocolType protocolType = ProtocolType.Tcp, int receiveBufferSize = NetworkAsyncState.DefaultReceiveBufferSize)
        {
            AddressFamily = addressFamily;
            SocketType = socketType;
            ProtocolType = protocolType;
            Encoding = encoding;
            ReceiveBufferSize = receiveBufferSize;

            connections = new List<Connection>();
            ServerState = ConnectionState.NotWorking;
        }

        /// <summary>
        /// デストラクタ
        /// </summary>
        ~Server()
        {
            Dispose(false);
        }

        /// <summary>
        /// 使用されているリソースの開放を行う
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 使用されているリソースの開放を行う
        /// </summary>
        /// <param name="disposing">アンマネージリソースとマネージリソースの両方開放するなら true / アンマネージリソースだけを解放する場合は false</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Close();

                Accepted = null;
                AcceptFailed = null;
                ClientClosed = null;
                DataReceived = null;

                connections = null;
                LocalEndPoint = null;
                Encoding = null;
            }
        }

        /// <summary>
        /// 接続を閉じる
        /// </summary>
        private void Close()
        {
            if (ServerState == ConnectionState.Closed)
            {
                return;
            }

            StopListening();
            CloseAllConnections();
            ServerState = ConnectionState.Closed;
        }

        /// <summary>
        /// 全ての確立されている接続を閉じる
        /// </summary>
        public void CloseAllConnections()
        {
            lock (SyncObject)
            {
                while (connections.Count > 0)
                {
                    CloseConnection(connections[0]);
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 接続を閉じる
        /// </summary>
        /// <param name="connection">閉じる接続</param>
        public void CloseConnection(Connection connection)
        {
            if (connections.Remove(connection))
            {
                connection.Dispose();
            }
            else
            {
                throw new InvalidOperationException("Invalid Connection.");
            }
        }

        /// <summary>
        /// 接続の受付を停止する
        /// </summary>
        public void StopListening()
        {
            if (ServerState != ConnectionState.Listening)
            {
                return;
            }

            lock (SyncObject)
            {
                listenSocket.Close();
                listenSocket = null;
                ServerState = ConnectionState.ListeningStopped;
            }
        }

        /// <summary>
        /// 接続の受付を開始する
        /// </summary>
        /// <param name="portNumber">利用するポート番号</param>
        /// <param name="backlog">保留中の接続のキューの最大長</param>
        public void StartListening(int portNumber = 50000, int backlog = 100)
        {
            if (ServerState == ConnectionState.Listening)
            {
                throw new InvalidOperationException("Already Listening.");
            }

            LocalEndPoint = new IPEndPoint(IPAddress.Any, portNumber);
            listenSocket = new Socket(AddressFamily, SocketType, ProtocolType);
            listenSocket.Bind(LocalEndPoint);
            listenSocket.Listen(backlog);
            listenSocket.BeginAccept(AcceptCallback, listenSocket);
            ServerState = ConnectionState.Listening;
        }

        /// <summary>
        /// 全てのクライアントにデータの送信を開始する
        /// </summary>
        /// <param name="sendData">送信するデータ</param>
        public void StartSendToAllClients(string sendData)
        {
            StartSendToAllClients(Encoding.GetBytes(sendData));
        }

        /// <summary>
        /// 全てのクライアントにデータの送信を開始する
        /// </summary>
        /// <param name="sendData">送信するデータ</param>
        public void StartSendToAllClients(byte[] sendData)
        {
            lock (SyncObject)
            {
                foreach (var connection in connections)
                {
                    connection.StartSending(sendData);
                }
            }
        }
        #endregion

        #region Callbacks
        /// <summary>
        /// Socket.BeginAccept のコールバック関数
        /// </summary>
        /// <param name="ar">非同期操作の結果</param>
        private void AcceptCallback(IAsyncResult ar)
        {
            if (ServerState == ConnectionState.Closed || ServerState == ConnectionState.ListeningStopped)
            {
                return;
            }

            var listenSocket = (ar.AsyncState as Socket);
            Socket acceptSocket = null;
            try
            {
                lock (SyncObject)
                {
                    acceptSocket = listenSocket.EndAccept(ar);
                }
            }
            catch (Exception e)
            {
                OnAcceptFailed(new AcceptFailedEventArgs(e));
                Close();
                return;
            }

            var connection = Connection.CreateFromSocket(acceptSocket, Encoding, ReceiveBufferSize);
            connection.Closed += (sender, e) =>
            {
                connections.Remove(e.Connection);
                OnClientClosed(e);
            };
            connection.DataReceived += DataReceived;
            connection.StartReceiving();
            connections.Add(connection);

            var eventArgs = new AcceptedEventArgs(connection);
            OnAccepted(eventArgs);

            if (eventArgs.ContinueListening)
            {
                listenSocket.BeginAccept(AcceptCallback, listenSocket);
            }
            else
            {
                StopListening();
            }
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// 接続を受け付けた時
        /// </summary>
        /// <param name="e">イベントデータ</param>
        private void OnAccepted(AcceptedEventArgs e)
        {
            if (Accepted != null)
            {
                Accepted(this, e);
            }
        }

        /// <summary>
        /// 接続の受け付けに失敗した時
        /// </summary>
        /// <param name="e">イベントデータ</param>
        private void OnAcceptFailed(AcceptFailedEventArgs e)
        {
            if (AcceptFailed != null)
            {
                AcceptFailed(this, e);
            }
        }

        /// <summary>
        /// クライアントの接続が閉じた時
        /// </summary>
        /// <param name="e">イベントデータ</param>
        private void OnClientClosed(ConnectionEventArgs e)
        {
            if (ClientClosed != null)
            {
                ClientClosed(this, e);
            }
        }
        #endregion
    }
}
