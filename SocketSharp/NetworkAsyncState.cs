﻿#region Using Statements
using System.Net.Sockets;
#endregion

namespace SocketSharp
{
    /// <summary>
    /// Socket.BeginReceive / BeginSend での非同期操作についての情報
    /// </summary>
    class NetworkAsyncState
    {
        /// <summary>
        /// デフォルトの受信バッファのサイズ
        /// </summary>
        public const int DefaultReceiveBufferSize = 1024;

        /// <summary>
        /// ソケット
        /// </summary>
        public Socket Socket { get; private set; }

        /// <summary>
        /// バッファ
        /// </summary>
        public byte[] Buffer { get; private set; }

        /// <summary>
        /// バッファサイズ
        /// </summary>
        public int BufferSize { get; private set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="socket">ソケット</param>
        public NetworkAsyncState(Socket socket)
            : this(socket, DefaultReceiveBufferSize)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="socket">ソケット</param>
        /// <param name="bufferSize">バッファサイズ</param>
        public NetworkAsyncState(Socket socket, int bufferSize)
            : this(socket, new byte[bufferSize])
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="socket">ソケット</param>
        /// <param name="buffer">バッファ</param>
        public NetworkAsyncState(Socket socket, byte[] buffer)
        {
            Socket = socket;
            Buffer = buffer;
            BufferSize = Buffer.Length;
        }
    }
}
