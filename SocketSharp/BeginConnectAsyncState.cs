﻿#region Using Statements
using System.Net;
using System.Net.Sockets;
#endregion

namespace SocketSharp
{
    /// <summary>
    /// Socket.BeginConnect での非同期操作についての情報
    /// </summary>
    class BeginConnectAsyncState
    {
        /// <summary>
        /// ソケット
        /// </summary>
        public Socket Socket { get; private set; }

        /// <summary>
        /// リモートエンドポイント
        /// </summary>
        public IPEndPoint RemoteEndPoint { get; private set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="socket">ソケット</param>
        /// <param name="remoteEndPoint">リモートエンドポイント</param>
        public BeginConnectAsyncState(Socket socket, IPEndPoint remoteEndPoint)
        {
            Socket = socket;
            RemoteEndPoint = remoteEndPoint;
        }
    }
}
