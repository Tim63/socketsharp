﻿#region Using Statements
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
#endregion

namespace SocketSharp
{
    /// <summary>
    /// ソケットの接続を制御するクラス
    /// </summary>
    public class Connection : IDisposable
    {
        #region Helper Class
        /// <summary>
        /// BeginGetHostEntry での非同期操作についての情報
        /// </summary>
        class BeginGetHostEntryAsyncState
        {
            public string Target { get; private set; }
            public int PortNumber { get; private set; }

            public BeginGetHostEntryAsyncState(string target, int portNumber)
            {
                Target = target;
                PortNumber = portNumber;
            }
        }
        #endregion

        #region Constants / Readonlies
        /// <summary>
        /// 同期用オブジェクト
        /// </summary>
        private readonly object SyncObject = new object();
        #endregion

        #region Events
        /// <summary>
        /// 接続に成功した時に発生するイベント
        /// </summary>
        public event EventHandler<ConnectionEventArgs> Connected;

        /// <summary>
        /// 接続に失敗した時に発生するイベント
        /// </summary>
        public event EventHandler<ConnectFailedEventArgs> ConnectFailed;

        /// <summary>
        /// 接続が閉じた時に発生するイベント
        /// </summary>
        public event EventHandler<ConnectionEventArgs> Closed;

        /// <summary>
        /// データを受信した時に発生するイベント
        /// </summary>
        public event EventHandler<DataReceivedEventArgs> DataReceived;
        #endregion

        #region Properties
        /// <summary>
        /// ローカルエンドポイント
        /// </summary>
        public IPEndPoint LocalEndPoint { get; private set; }

        /// <summary>
        /// リモートエンドポイント
        /// </summary>
        public IPEndPoint RemoteEndPoint { get; private set; }

        /// <summary>
        /// アドレスファミリ
        /// </summary>
        public AddressFamily AddressFamily { get; private set; }

        /// <summary>
        /// ソケットの種類
        /// </summary>
        public SocketType SocketType { get; private set; }

        /// <summary>
        /// 接続プロトコルの種類
        /// </summary>
        public ProtocolType ProtocolType { get; private set; }

        /// <summary>
        /// 利用する文字エンコーディング
        /// </summary>
        public Encoding Encoding { get; private set; }

        /// <summary>
        /// 受信バッファサイズ
        /// </summary>
        public int ReceiveBufferSize { get; private set; }

        /// <summary>
        /// 接続状態
        /// </summary>
        public ConnectionState State { get; private set; }
        #endregion

        #region Fields
        /// <summary>
        /// この接続で利用するソケット
        /// </summary>
        private Socket socket;
        #endregion

        #region Create / Dispose
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="addressFamily">アドレスファミリ</param>
        /// <param name="socketType">ソケットの種類</param>
        /// <param name="protocolType">接続プロトコルの種類</param>
        /// <param name="receiveBufferSize">受信バッファサイズ</param>
        public Connection(AddressFamily addressFamily = AddressFamily.InterNetwork, SocketType socketType = SocketType.Stream, ProtocolType protocolType = ProtocolType.Tcp, int receiveBufferSize = NetworkAsyncState.DefaultReceiveBufferSize)
            : this(Encoding.UTF8, addressFamily, socketType, protocolType, receiveBufferSize)
        {
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="encoding">文字エンコーディング</param>
        /// <param name="addressFamily">アドレスファミリ</param>
        /// <param name="socketType">ソケットの種類の種類</param>
        /// <param name="protocolType">接続プロトコル</param>
        /// <param name="receiveBufferSize">受信バッファサイズ</param>
        public Connection(Encoding encoding, AddressFamily addressFamily = AddressFamily.InterNetwork, SocketType socketType = SocketType.Stream, ProtocolType protocolType = ProtocolType.Tcp, int receiveBufferSize = NetworkAsyncState.DefaultReceiveBufferSize)
        {
            AddressFamily = addressFamily;
            SocketType = socketType;
            ProtocolType = protocolType;
            Encoding = encoding;
            ReceiveBufferSize = receiveBufferSize;

            State = ConnectionState.NotWorking;
        }

        /// <summary>
        /// ソケットから接続を作成
        /// </summary>
        /// <param name="socket">ソケット</param>
        /// <param name="encoding">文字エンコーディング</param>
        /// <param name="receiveBufferSize">受信バッファサイズ</param>
        /// <returns>作成した接続</returns>
        internal static Connection CreateFromSocket(Socket socket, Encoding encoding, int receiveBufferSize = NetworkAsyncState.DefaultReceiveBufferSize)
        {
            var connection = new Connection(encoding, socket.AddressFamily, socket.SocketType, socket.ProtocolType);
            connection.socket = socket;
            connection.LocalEndPoint = socket.LocalEndPoint as IPEndPoint;
            connection.RemoteEndPoint = socket.RemoteEndPoint as IPEndPoint;
            connection.State = socket.Connected ? ConnectionState.Connected : ConnectionState.NotWorking;
            return connection;
        }

        /// <summary>
        /// デストラクタ
        /// </summary>
        ~Connection()
        {
            Dispose(false);
        }

        /// <summary>
        /// 使用されているリソースの開放を行う
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// 使用されているリソースの開放を行う
        /// </summary>
        /// <param name="disposing">アンマネージリソースとマネージリソースの両方開放するなら true / アンマネージリソースだけを解放する場合は false</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Close();

                Connected = null;
                ConnectFailed = null;
                Closed = null;
                DataReceived = null;

                LocalEndPoint = null;
                RemoteEndPoint = null;
                Encoding = null;
            }
        }

        /// <summary>
        /// 接続を閉じる
        /// </summary>
        private void Close()
        {
            if (State == ConnectionState.Closed)
            {
                return;
            }

            lock (SyncObject)
            {
                var isWorking = State != ConnectionState.NotWorking;
                if (isWorking)
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                    socket = null;
                }
                State = ConnectionState.Closed;

                if (isWorking)
                {
                    OnClosed(new ConnectionEventArgs(this));
                }
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 接続を開始する
        /// </summary>
        /// <param name="target">接続先となるホスト名 / IP アドレス</param>
        /// <param name="portNumber">利用するポート番号</param>
        public void StartConnecting(string target, int portNumber = 50000)
        {
            if (State == ConnectionState.Connected)
            {
                throw new InvalidOperationException("Already Connected.");
            }

            try
            {
                IPAddress ipAddress;
                if (!IPAddress.TryParse(target, out ipAddress))
                {
                    Dns.BeginGetHostEntry(target, GetHostEntryCallback, new BeginGetHostEntryAsyncState(target, portNumber));
                }
                else
                {
                    BeginConnect(ipAddress, portNumber);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        /// <summary>
        /// データの受信を開始する
        /// </summary>
        public void StartReceiving()
        {
            if (State != ConnectionState.Connected)
            {
                throw new InvalidOperationException("Not Connected.");
            }

            try
            {
                lock (SyncObject)
                {
                    var state = new NetworkAsyncState(socket);
                    socket.BeginReceive(state.Buffer, 0, ReceiveBufferSize, SocketFlags.None, ReceiveCallback, state);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        /// <summary>
        /// データの送信を開始する
        /// </summary>
        /// <param name="sendData">送信するデータ</param>
        public void StartSending(byte[] sendData)
        {
            if (State != ConnectionState.Connected)
            {
                throw new InvalidOperationException("Not Connected.");
            }

            try
            {
                lock (SyncObject)
                {
                    var state = new NetworkAsyncState(socket, sendData);
                    socket.BeginSend(state.Buffer, 0, state.BufferSize, SocketFlags.None, SendCallback, state);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        /// <summary>
        /// データの送信を開始する
        /// </summary>
        /// <param name="sendData">送信するデータ</param>
        public void StartSending(string sendData)
        {
            StartSending(Encoding.GetBytes(sendData));
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// 接続を開始する
        /// </summary>
        /// <param name="address">IP アドレス</param>
        /// <param name="portNumber">ポート番号</param>
        private void BeginConnect(IPAddress address, int portNumber)
        {
            lock (SyncObject)
            {
                socket = new Socket(AddressFamily, SocketType, ProtocolType);
                var state = new BeginConnectAsyncState(socket, new IPEndPoint(address, portNumber));
                socket.BeginConnect(state.RemoteEndPoint, ConnectCallback, state);
            }
        }

        #endregion

        #region Callbacks
        /// <summary>
        /// Dns.BeginGetHostEntry のコールバック関数
        /// </summary>
        /// <param name="ar">非同期操作の結果</param>
        private void GetHostEntryCallback(IAsyncResult ar)
        {
            IPHostEntry ipHostEntry = null;
            try
            {
                ipHostEntry = Dns.EndGetHostEntry(ar);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return;
            }

            IPAddress address = null;
            foreach (var ipAddress in ipHostEntry.AddressList)
            {
                if (ipAddress.AddressFamily == AddressFamily)
                {
                    address = ipAddress;
                    break;
                }
            }
            if (address == null)
            {
                throw new Exception("Target NOT Found.");
            }

            BeginConnect(address, (ar.AsyncState as BeginGetHostEntryAsyncState).PortNumber);
        }

        /// <summary>
        /// Socket.BeginConnect のコールバック関数
        /// </summary>
        /// <param name="ar">非同期操作の結果</param>
        private void ConnectCallback(IAsyncResult ar)
        {
            if (State == ConnectionState.Closed)
            {
                return;
            }

            var state = (ar.AsyncState as BeginConnectAsyncState);
            var socket = state.Socket;
            try
            {
                socket.EndConnect(ar);
            }
            catch (Exception e)
            {
                OnConnectFailed(new ConnectFailedEventArgs(this, state.RemoteEndPoint, e));
                return;
            }

            LocalEndPoint = socket.LocalEndPoint as IPEndPoint;
            RemoteEndPoint = socket.RemoteEndPoint as IPEndPoint;
            State = ConnectionState.Connected;
            OnConnected(new ConnectionEventArgs(this));

            StartReceiving();
        }

        /// <summary>
        /// Socket.BeginReceive のコールバック関数
        /// </summary>
        /// <param name="ar">非同期操作の結果</param>
        private void ReceiveCallback(IAsyncResult ar)
        {
            int dataLength = -1;
            try
            {
                lock (SyncObject)
                {
                    dataLength = socket.EndReceive(ar);
                }
            }
            catch { }

            if (dataLength <= 0)
            {
                Close();
                return;
            }

            var state = ar.AsyncState as NetworkAsyncState;

            OnDataReceived(new DataReceivedEventArgs(this, state.Buffer, dataLength));

            lock (SyncObject)
            {
                state.Socket.BeginReceive(state.Buffer, 0, ReceiveBufferSize, SocketFlags.None, ReceiveCallback, state);
            }
        }

        /// <summary>
        /// Socket.BeginSend のコールバック関数
        /// </summary>
        /// <param name="ar">非同期操作の結果</param>
        private void SendCallback(IAsyncResult ar)
        {
            var state = ar.AsyncState as NetworkAsyncState;
            try
            {
                var sendLength = state.Socket.EndSend(ar);
                if (sendLength != state.Buffer.Length)
                {
                    Console.WriteLine("Send Failed. Buffer Size: {0}, Send Size: {1}", state.Buffer.Length, sendLength);
                    Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Send Failed. Exception: " + e.ToString());
                Close();
            }
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// 接続に成功した時
        /// </summary>
        /// <param name="e">イベントデータ</param>
        private void OnConnected(ConnectionEventArgs e)
        {
            if (Connected != null)
            {
                Connected(this, e);
            }
        }

        /// <summary>
        /// 接続に失敗した時
        /// </summary>
        /// <param name="e">イベントデータ</param>
        private void OnConnectFailed(ConnectFailedEventArgs e)
        {
            if (ConnectFailed != null)
            {
                ConnectFailed(this, e);
            }
        }

        /// <summary>
        /// 接続が閉じた時
        /// </summary>
        /// <param name="e">イベントデータ</param>
        private void OnClosed(ConnectionEventArgs e)
        {
            if (Closed != null)
            {
                Closed(this, e);
            }
        }

        /// <summary>
        /// データを受信した時
        /// </summary>
        /// <param name="e">イベントデータ</param>
        private void OnDataReceived(DataReceivedEventArgs e)
        {
            if (DataReceived != null)
            {
                DataReceived(this, e);
            }
        }
        #endregion
    }
}
