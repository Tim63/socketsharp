﻿#region Using Statements
using System;
using System.Net;
#endregion

namespace SocketSharp
{
    /// <summary>
    /// 接続に失敗した時のイベントデータ
    /// </summary>
    public class ConnectFailedEventArgs : ConnectionEventArgs
    {
        /// <summary>
        /// リモートエンドポイント
        /// </summary>
        public IPEndPoint RemoteEndPoint { get; private set; }

        /// <summary>
        /// 発生した例外
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="connection">接続</param>
        /// <param name="remoteEndPoint">リモートエンドポイント</param>
        /// <param name="exception">発生した例外</param>
        public ConnectFailedEventArgs(Connection connection, IPEndPoint remoteEndPoint, Exception exception)
            : base(connection)
        {
            RemoteEndPoint = remoteEndPoint;
            Exception = exception;
        }
    }

}
