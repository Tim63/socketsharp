﻿#region Using Statements
using System;
#endregion

namespace SocketSharp
{
    /// <summary>
    /// データを受信した時のイベントデータ
    /// </summary>
    public class DataReceivedEventArgs : ConnectionEventArgs
    {
        /// <summary>
        /// 受信したデータ
        /// </summary>
        public byte[] ReceivedData { get; private set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="connection">接続</param>
        /// <param name="receivedData">受信したデータ</param>
        /// <param name="dataLength">受信したデータの長さ</param>
        public DataReceivedEventArgs(Connection connection, byte[] receivedData, int dataLength)
            : base(connection)
        {
            ReceivedData = new byte[dataLength];
            Array.Copy(receivedData, ReceivedData, dataLength);
        }
    }
}
