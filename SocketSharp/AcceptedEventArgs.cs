﻿#region Using Statements
using System;
#endregion

namespace SocketSharp
{
    /// <summary>
    /// 接続の受け付けに成功した時のイベントデータ
    /// </summary>
    public class AcceptedEventArgs : ConnectionEventArgs
    {
        /// <summary>
        /// 接続の受け付けを継続するかどうか
        /// </summary>
        public bool ContinueListening { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="connection">接続</param>
        public AcceptedEventArgs(Connection connection)
            : base(connection)
        {
            ContinueListening = true;
        }
    }
}
