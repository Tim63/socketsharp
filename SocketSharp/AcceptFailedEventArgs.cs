﻿#region Using Statements
using System;
#endregion

namespace SocketSharp
{
    /// <summary>
    /// 接続の受け付けに失敗した時のイベントデータ
    /// </summary>
    public class AcceptFailedEventArgs : EventArgs
    {
        /// <summary>
        /// 発生した例外
        /// </summary>
        public Exception Exception { get; private set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="exception">発生した例外</param>
        public AcceptFailedEventArgs(Exception exception)
        {
            Exception = exception;
        }
    }
}
