﻿#region Using Statements
using System;
#endregion

namespace SocketSharp
{
    /// <summary>
    /// 接続のイベントデータ
    /// </summary>
    public class ConnectionEventArgs : EventArgs
    {
        /// <summary>
        /// 接続
        /// </summary>
        public Connection Connection { get; private set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="connection">接続</param>
        public ConnectionEventArgs(Connection connection)
        {
            Connection = connection;
        }
    }
}
