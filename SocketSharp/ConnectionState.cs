﻿
namespace SocketSharp
{
    /// <summary>
    /// 接続の状態
    /// </summary>
    public enum ConnectionState
    {
        /// <summary>
        /// 作動していない
        /// </summary>
        NotWorking,

        /// <summary>
        /// 接続されている
        /// </summary>
        Connected,

        /// <summary>
        /// 接続待ちしている
        /// </summary>
        Listening,

        /// <summary>
        /// 接続待ちを終了している
        /// </summary>
        ListeningStopped,

        /// <summary>
        /// 接続が閉じている
        /// </summary>
        Closed,
    }
}
